import urllib.request
import re
import mpv
import os
import glob
import os.path
import random
import csv
from more_itertools import unique_everseen
import shutil
from tinytag import TinyTag
from yt_dlp import YoutubeDL
import yt_dlp
ydl = YoutubeDL()
ydl.add_default_info_extractors


class BackEndControls(object):

    # constractor of the class
    def __init__(self, FileType, ydl, url, quality, playListMpv):
        self.FileType = None
        self.ydl = None
        self.url = None
        self.quality = None
        self.playListMpv = [None]

    def yt(self , plmode ,singlem):
        urllist=[""]
        qualitylist=[""]
        if singlem == True:
            plmode=True
            
        

        while plmode==True or singlem == True:
           
            ydl = YoutubeDL()
            ydl.add_default_info_extractors

            # get the info from the user
            search = input("search ... \n")
            # read the html of the page and fined the urls
            html = urllib.request.urlopen(
                "https://www.youtube.com/results?search_query=" + search.replace(" ", "+"))
            video_ids = re.findall(r"watch\?v=(\S{11})", html.read().decode())
            # check if the input is integer
            while True:
                try:
                    a = int(input("How many results do you want ?\n"))
                    break
                except ValueError:
                    print("please give a integer")
            i = -1

            urls = []
            # loop to fill the urls array to pass the urls to the yt-dlp
            for x in range(int(a)):
                i = i+1

                print("https://www.youtube.com/watch?v=" + video_ids[i])
                urls.append("https://www.youtube.com/watch?v=" + video_ids[i])
                # input the urls in ydl1(youtube-dl)

                with ydl:
                    result = ydl.extract_info(urls[i], download=False,)
                    title = result['title']
                    print(i, title)

            while True:
                try:
                    choose = int(
                        input("choose the video you want to play from the above\n"))
                    break
                except ValueError:
                    print("please give an integer")
            url = urls[int(choose)]
            # choose the quality for the video and audio
            print("choose qulaity to for the video and audio")
            # options for the video selected by the user
            options = {
                'format': 'bestaudio/best',  # choice of quality
                'extractaudio': True,        # only keep the audio
                'audioformat': "mp3",        # convert to mp3
                'outtmpl': '%(id)s',         # name the file the ID of the video
                'noplaylist': True,          # only download single song, not playlist
                'listformats': True,

            }
            with yt_dlp.YoutubeDL(options) as ydl:
                ydl.download([url])
            # select the video quality
            merge = None
            # Check if the user gives posible input
            while merge != "y" and merge != "n":

                merge = input(
                    "you want to merge some format of audio with some format of video y/n\n")
                if merge != "y" and merge != "n":
                    print("please eneter y or n ")
            if merge == "y":

                while True:
                    try:
                        quality = str(input(
                            "choose quality for video and audio and insert in the form of 'video+audio'\n"))
                        break
                    except ValueError:
                        print("this is not number please choose a number from above matrix")
            if merge == "n":
                print("you are in no merge mode")
                print("choose single quality ")
                quality = input(
                    "if you want the full quality of audio and video then just press enter or choose by number\n")
            urllist.append(url)
            qualitylist.append(quality)
            
            if singlem ==True:
                plmode=False
                singlem=False
                
            
            else:
                urlloadbreak=input("you want to save another url in playlist? y/n")
                if urlloadbreak == 'y' and singlem==False:
                    print("this is and with y")
                    plmode=True
                if urlloadbreak == 'n' and singlem==False:
                    print("this is n with and")
                    plmode=False

        return quality, url ,urls ,urllist , qualitylist
       
    #this is a method to play the media
    def Playing(self, url, quality, ytdlst, playListMpv ,qualitylist):
        player = mpv.MPV(ytdl=ytdlst, ytdl_format=quality, input_vo_keyboard=True,input_default_bindings=True,osc=True)
        #play one file
        if url != None and url != "ytpl":
            #try:
            print("this is the quality " +quality)
            #print("this is the ytdst "+ytdlst)
            print("play one file")
            print("this is the url " +url)
            try:
                print("playing")
                player.play(url)
                player.wait_for_playback()
            except Exception as e:
                print (e)
            print("player closed")
            print("this app supports sites from this list only ")
            print("--------------------------------------------------")
            print("https://github.com/yt-dlp/yt-dlp/blob/master/supportedsites.md")
            print("--------------------------------------------------")
         
        #play playlist from other site other then YouTube
        if url == None:
            i=0
            a=len(playListMpv)
           
            shuffle=input("do you want random order ?")
            for x in range(a):
                
                try:
                    player.playlist_append(playListMpv[i])
                except:
                    print("error in plailist")
                #print(player.playlist)
                i = i+1
                #this is for play in random order
            try:    
                if shuffle=="y":
                    player.playlist_shuffle()
                    player.playlist_pos = 0
                    print("this app supports sites from this list only ")
                    print("--------------------------------------------------")
                    print("https://github.com/yt-dlp/yt-dlp/blob/master/supportedsites.md")
                    print("--------------------------------------------------")

                elif shuffle=="n":
                    player.playlist_pos = 0
                    print("this app supports sites from this list only ")
                    print("--------------------------------------------------")
                    print("https://github.com/yt-dlp/yt-dlp/blob/master/supportedsites.md")
                    print("--------------------------------------------------")
                else:
                    print("auto mode")
                    player.playlist_pos = 0 
                    print("this app supports sites from this list only ")
                    print("--------------------------------------------------")
                    print("https://github.com/yt-dlp/yt-dlp/blob/master/supportedsites.md")
                    print("--------------------------------------------------")
                while True:
                   player.wait_for_playback()
            except:
                print("the player just close")
               
        #play playlist from YouTube
        if url == 'ytpl':
            try:
                qualitylist.remove("")
            except:
                print("all good")
            try:
                playListMpv.remove("")
            except:
                print("all good")
            z=len(qualitylist)
            shuffle=input("do you want to play at random order ? \n")
            a=0
            if shuffle=="y":
                random.shuffle(playListMpv)
                print("you have selected the random order \n")
            else:
                print("play at normal order\n")
            for x in qualitylist:
              
                player = mpv.MPV(ytdl=ytdlst, ytdl_format=quality, input_vo_keyboard=True,input_default_bindings=True,osc=True)
                player.play(playListMpv[a])
                player.wait_for_playback()
                print("this app supports sites from this list only ")
                print("--------------------------------------------------")
                print("https://github.com/yt-dlp/yt-dlp/blob/master/supportedsites.md")
                print("--------------------------------------------------")
                a=a+1
               

    def SelectMethod(self):

        # save the user input
        FileType = input(" You want to play a file , url ,custom_file or from youtube ?\n")

        # check if the user input is valid
        while FileType != "youtube" and FileType != "url" and FileType != "file" and FileType!= "custom_file":
            FileType = input(
                "you want to play video from file , url , youtube , custom_file?\n")
        if FileType != "youtube" and FileType != "url" and FileType != "file" and FileType != "custom_file":
            print("error try again")
        return FileType

    # method to take the url from the user
    def TakeUrlMethod(self):
        url = input("paste here your url from the supported sites\n")
        return url
    # method to take the file path from the user

    def TakeLocalFilePath(self):
        url = input("paste here the location for the localfile\n")
        
        return url

    # method to make a playlist
    def MakePlaylist(self):
        print("this is the playlist mode ")
        x = list(map(str, input("enter your paths/links: ").split(",")))
        return x

    # method to show metada
    def ShowMetada(self, yturls ,mode):
        artists=["None"]
        titles=["None"]
        song_names=["None"]
        quetions=["None"]
        artist="None"
        title="None"
        song_name="None"
        quetion="None"
        if mode=="pltru":
            yturls.remove('')
     
            i = len(yturls)
            a=0
            #for youtube metada
            for x in range(i):
                
                with ydl:
                    result = ydl.extract_info(yturls[a], download=False,)
                    try:

                        song_name = result["track"]

                        print("name of the song  \n"+song_name)
                        song_names.append(song_name)
                    except:
                        print("error load the name")
                    try:
                        artist = result["artist"]
                        print(" artist \n"+artist)
                        artists.append(artist)
                    except:
                        print ("error loading the artist")
                      
                    try:
                        title = result['title']
                        print(i, title)
                        titles.append(title)
                    except:
                        print("error loading the title")
                        album=result['album']
                    try:
                        print("album" + album)
                        print(i, title)
                    except:
                        print("error loading the album")
                        
                        description = result['description']
                        print("this is the description  \n" + description)
                    quetion=input("do you have a comment on this track")
                    quetions.append(quetion)
                    #artists.remove("None")
                    #song_names.remove("None")
                    #titles.remove("None")
                    #quetions.remove("None")
                    
                        
                    a=a+1
            

            
        if mode=="plfalse":
            
            with ydl:
                result = ydl.extract_info(yturls, download=False,)
                try:
                    song_name = result["track"]
                    print("name of the song  \n"+song_name)
                    song_names.append(song_name)
                except:
                    print("error load the name")
                try:
                    artist = result["artist"]
                    print(" artist \n"+artist)
                    artists.append(artist)
                except:
                    print ("error loading the artist")
                  
                try:
                    title = result['title']
                    print(i, title)
                    titles.append(title)
                except:
                    print("error loading the title")
                    
                try:
                    album=result['album']
                    print("album" + album)
                    print(i, title)
                except:
                    print("error loading the album")
                    
                    description = result['description']
                    print("this is the description  \n" + description)
                quetion=input("do you have any comment ?")
                
                
        return titles,artists,song_names,title,artist,song_name,quetion,quetions
    #method to read links from files            
    def ReadFromFile(self):
        htmfile=input("Give me the text file:")
        bookmark=open(htmfile,"r")
        #bookmark=open("/media/musicfan/hdd/doc/YoutubePlayer/Ytplayer/Ytplayer/balance.html","r")
        x=0
        custFile=video_ids = re.findall(r"https?://(?:[-\w./*?=])+", bookmark.read())
        cutsList=[]
        for i in video_ids:
          
            
            cutsList.append(video_ids[x])
            print(video_ids[x])
            x=x+1
        print("This is the number of results")
        print(x)
        return cutsList
        
            
            
    #method to show the audio metada
    def audiometadata(self ,path ,itemlist ,pllist):
        comms=["None"]
        if pllist==False:
            audio = TinyTag.get(path)
            Title=str((audio.title))
            print("Title:" + Title)
            Artist=str(audio.artist)
            print("Artist: " + Artist )
            Genre=str((audio.genre))
            print("Genre:" + Genre)
            Year=str(audio.year)
            print("Year Released: " +  Year)
            Bitrate=audio.bitrate
            print("Bitrate:" + str(Bitrate) + " kBits/s")
            Composer=audio.composer
            print("Composer: " + str(Composer))
            Filesize=audio.filesize
            print("Filesize: " + str(Filesize) + " bytes")
            Albumartist=audio.albumartist
            print("AlbumArtist: " + str(Albumartist))
            Duration=audio.duration
            print("Duration: " + str(Duration) + " seconds")
            TrackTotal=audio.track_total
            print("TrackTotal: " +str(TrackTotal))
            comm=input("any comment")
        Titles=["None"]
        Artists=["None"]
        Genres=["None"]
        Years=["None"] 
        
        #this is a list for the playlist option 
     
        if pllist==True:
            
            
            Title="None"
            Artist="None"
            Genre="None" 
            Year="None"
            a=len(itemlist)
            i=0
         
            Titles.remove("None")
            Artists.remove("None")
            Genres.remove("None")
            Years.remove("None")
            comms.remove("None")
            sep="'"
            sep1="'"
            
            for k in range(a):
                
              
                audio = TinyTag.get(itemlist[i])
                Title=str((audio.title))
                print("Title:" + Title)
                Title1=sep+Title+sep1
                Artist=str(audio.artist)
                print("Artist: " + Artist )
                Genre=str((audio.genre))
                print("Genre:" + Genre)
                Year=str(audio.year)
                print("Year Released: " +  Year)
                Bitrate=audio.bitrate
                print("Bitrate:" + str(Bitrate) + " kBits/s")
                Composer=audio.composer
                print("Composer: " + str(Composer))
                Filesize=audio.filesize
                print("Filesize: " + str(Filesize) + " bytes")
                Albumartist=audio.albumartist
                print("AlbumArtist: " + str(Albumartist))
                Duration=audio.duration
                print("Duration: " + str(Duration) + " seconds")
                TrackTotal=audio.track_total
                print("TrackTotal: " + str(TrackTotal))
                #loading the list for the metadata
               
                
                Titles.append(Title)
                Artists.append(Artist)
                Genres.append(Genre)
                Years.append(Year)

                comm=input("any comment")
                comms.append(comm)
                i=i+1
                 
        return Title,Artist,Genre,Year,Titles,Artists,Genres,Years,comm,comms      
        
    #this is a method to load a directory in a list
    def loadonlycomp(self , listdir):
        dir = listdir
        mp4s=(glob.glob(os.path.join(dir, '*.mp4')))
        mp3s=(glob.glob(os.path.join(dir, '*.mp3')))
        avis=(glob.glob(os.path.join(dir, '*.avi')))
        movs=(glob.glob(os.path.join(dir, '*.mov')))
        flacs=(glob.glob(os.path.join(dir, '*.flac')))
        aacs=(glob.glob(os.path.join(dir, '*.acc')))
        wavs=(glob.glob(os.path.join(dir, '*.wav')))
        wmas=(glob.glob(os.path.join(dir, '*.wma')))
        aifs=((glob.glob(os.path.join(dir, '*.aif'))))
        files=[]
        
        if mp3s!=[]:
            files.extend(mp3s)
        if mp4s!=[]:
            files.extend(mp4s)
        if avis!=[]:
            files.extend(avis)
        if movs!=[]:
            files.extend(movs)
        if flacs!=[]:
            files.extend(flacs)
           
        if aacs!=[]:
            files.extend(aacs)
        if wavs!=[]:
            files.extend(wavs)
        if wmas!=[]:
            files.extend(wmas)
        if aifs!=[]:
            files.extend(aifs)
      
        
        print("this is the z" +str(files))
        #x = list(map(str,files.split(",")))
        
        
        return files
    #this is the method to load the hystory
    def LoadTheHystory(self):
        
        #load csv and convert to the dictionery
        filename="./output.csv"
        artist=[None]
        title=[None]
        genre=[None]
        year=[None]
        artists=[None]
        titles=[None]
        genres=[None]
        years=[None]
        i=0
        with open(filename, mode='r') as inp:
            for line in csv.reader(inp):
                
                title.append(line[0])
                
                artist.append(line[1])
                genre.append(line[2])
                year.append(line[3])
                i=i+1
            title.remove(None)
            artist.remove(None)
            genre.remove(None)
            year.remove(None)
            artistkey=artist[0]
            titlekey=title[0]
            genrekey=genre[0]
            yearkey=year[0]
            a=len(title)
            
            x=0
            for i in range(a-1):
                x=x+1
                artists.append(artist[x])
                titles.append(title[x])
                genres.append(genre[x])
                years.append(year[x])
            titles.remove(None)
            artists.remove(None)
            genres.remove(None)
            years.remove(None)    
            hystory={artistkey:[],titlekey: [],genrekey:[],yearkey:[]}
            b=len(titles)
            for c in range(b):
                hystory[artistkey].append(artists[c])
                hystory[titlekey].append(titles[c])
                hystory[genrekey].append(genres[c])
                hystory[yearkey].append(years[c])
            artistlist=hystory[artistkey]
            titlelist=hystory[titlekey]
            genrelist=hystory[genrekey]
            yearlist=hystory[yearkey]
        return artistlist,titlelist,genrelist,yearlist,hystory

    #this is the method to update the hystory file
    def UpdateTheHystory(self,song_name,artist,genre,year,pllist,comment):
        
       
        
        try:
            song_name.remove("None")
        except:
            print("ok")
        try:
            artist.remove("None")
        except:
            print("ok")
        try:
            genre.remove("None")
        except:
            print("ok")
        try:
            comment.remove("None")
        except:
            print("ok")
        if pllist==True:
            base={'Title':None,'Artist':None,'genre':None,'year':None ,'comments':None}
            h=0
            z=len(song_name)
           
            
            for x in range(z):
                try:
                    update={'Title':song_name[x],'Artist':artist[x],'genre':genre[x],'year': year[x] ,'comments':comment[x]}
                    base.update(update)
                    #print(base)
                    #write in the file
                    w = csv.writer(open("output.csv", "a"))
                    #w.writerow(base.keys())
                    w.writerow(base.values())
                    h=h+1
                    
                except:
                    print("balance")

        if pllist==False:
            
            base={'Title':None,'Artist':None,'genre':None,'year':None, 'comments':None}
            update={'Title':song_name,'Artist':artist,'genre':genre,'year': year,'comments':comment}
            base.update(update)
            #print(base)
            
            #write in the file
            w = csv.writer(open("output.csv", "a"))
            #w.writerow(base.keys())
            w.writerow(base.values())
    #this is a method to read the csv file and print the artist in quetion 
    def searchhystory(self,filename,searchartist,plst,list):
        filename="clean.csv"
        artist=[None]
        title=[None]
        genre=[None]
        year=[None]
        comment=[None]
        artists=[None]
        titles=[None]
        genres=[None]
        years=[None]
        comments=[None]
        
        
        if plst=="off":
            

            with open(filename, mode='r') as inp:
                for line in csv.reader(inp):
                    title.append(line[0])
                    artist.append(line[1])
                    genre.append(line[2])
                    year.append(line[3])
                    comment.append(line[4])
                title.remove(None)
                artist.remove(None)
                genre.remove(None)
                year.remove(None)
                comment.remove(None)
                artistkey=artist[0]
                titlekey=title[0]
                genrekey=genre[0]
                yearkey=year[0]
                commentkey=comment[0]
                a=len(title)

                x=0
                for i in range(a-1):
                    x=x+1
                    artists.append(artist[x])
                    titles.append(title[x])
                    genres.append(genre[x])
                    years.append(year[x])
                    comments.append(comment[x])
                titles.remove(None)
                artists.remove(None)
                genres.remove(None)
                years.remove(None)
                comments.remove(None)    
                hystory={artistkey:[],titlekey: [],genrekey:[],yearkey:[],commentkey:[]}
                b=len(titles)
                for c in range(b):
                    hystory[artistkey].append(artists[c])
                    hystory[titlekey].append(titles[c])
                    hystory[genrekey].append(genres[c])
                    hystory[yearkey].append(years[c])
                    hystory[commentkey].append(comment[c])
                artistlist=hystory[artistkey]
                titlelist=hystory[titlekey]
                genrelist=hystory[genrekey]
                yearlist=hystory[yearkey]
                commentlist=hystory[commentkey]
                #searchartist=artist
                index=[]
                for target, elem in enumerate(artistlist):
                    if elem==searchartist:

                        print(f"{searchartist} is found at index {target}")
                        index.append(target)
                i=0
                print("this is the hystory for the artist you listened "+ str(searchartist))
                for a in index:
                    print(searchartist,i)
                    print("-----------")
                    print(artistlist[index[i]])
                    print(titlelist[index[i]])
                    print(genrelist[index[i]])
                    print(yearlist[index[i]])
                    print(commentlist[index[i]])
                    print(searchartist)
                    i=i+1
            inp.close()
        if plst=="on":
             with open(filename, mode='r') as inp:
                for line in csv.reader(inp):
                    
                    title.append(line[0])
                    artist.append(line[1])
                    genre.append(line[2])
                    year.append(line[3])
                    comment.append(line[4])
                title.remove(None)
                artist.remove(None)
                genre.remove(None)
                year.remove(None)
                comment.remove(None)
                artistkey=artist[0]
                titlekey=title[0]
                genrekey=genre[0]
                yearkey=year[0]
                commentkey=comment[0]
                a=len(title)

                x=0
                for i in range(a-1):
                    x=x+1
                    artists.append(artist[x])
                    titles.append(title[x])
                    genres.append(genre[x])
                    years.append(year[x])
                    comments.append(comment[x])
                titles.remove(None)
                artists.remove(None)
                genres.remove(None)
                years.remove(None)
                comments.remove(None)    
                hystory={artistkey:[],titlekey: [],genrekey:[],yearkey:[],commentkey:[]}
                b=len(titles)
                for c in range(b):
                    hystory[artistkey].append(artists[c])
                    hystory[titlekey].append(titles[c])
                    hystory[genrekey].append(genres[c])
                    hystory[yearkey].append(years[c])
                    hystory[commentkey].append(comment[c])
                artistlist=hystory[artistkey]
                titlelist=hystory[titlekey]
                genrelist=hystory[genrekey]
                yearlist=hystory[yearkey]
                commentlist=hystory[commentkey]
                #searchartist=artist
                ba = len(list)
                for w in range(ba):
                    index=[]
                    for target, elem in enumerate(artistlist):
                        if elem==list[w]:

                            print(f"{list[w]} is found at index {target}")
                            index.append(target)
                    i=0
                    print("this is the hystory for the artist you listened "+ str(list[w]))
                    for a in index:
                        print(list,i)
                        print("-----------")
                        print(artistlist[index[i]])
                        print(titlelist[index[i]])
                        print(genrelist[index[i]])
                        print(yearlist[index[i]])
                        print(commentlist[index[i]])
                    
                        i=i+1
            

          

        return artist,title,genre,year,artists,titles,genres,years

    #this is a methot to clear the hystory for dubles entrys
    def clearThehystory(self):
        with open('output.csv','r') as f, open('clean.csv','w') as out_file:
            out_file.writelines(unique_everseen(f))
        shutil.copyfile("clean.csv", "output.csv")
    
    


    

        
# create object of the class BackEndControler
mplayer = BackEndControls(url=None, FileType=None,
                          ydl=None, quality=None, playListMpv="")
# call the method to select mode
selmod = mplayer.SelectMethod() 
        
# check the mode and start the methods respectevly

# this is for youtube
if selmod == "youtube":
    print("Υou have selected ΥouΤube mode")
    playlistyn=input("Do you want to make a playlist ? y/n")
    if playlistyn == 'y':
        
        metadatach=input("do you want to see metadata from the links you have choose ? \n")
        ytplaylist=mplayer.yt(plmode=True ,singlem=False)
        if metadatach == 'y':
            meta=mplayer.ShowMetada(ytplaylist[3],mode="pltru")
            oldhyst=mplayer.LoadTheHystory()
            
            mplayer.UpdateTheHystory(song_name=meta[2],artist=meta[1],genre="None",year="None",comment=meta[7],pllist=True)
            mplayer.searchhystory(filename="clean.csv",searchartist=meta[2],plst="on",list=meta[1])
            
            mplayer.clearThehystory()           
            mplayer.Playing(url="ytpl",quality=ytplaylist[0],ytdlst=True,playListMpv=ytplaylist[3], qualitylist=ytplaylist[4])
            
            
            
        else:
            print("no metadata will be loaded")
            mplayer.Playing(url="ytpl",quality=ytplaylist[0],ytdlst=True,playListMpv=ytplaylist[3] ,qualitylist=ytplaylist[4])
            

    elif playlistyn == 'n':
        yturl = mplayer.yt(plmode=False , singlem=True)
        metadatach=input("do you want to see metadata from the links you have choose ? \n")
        if metadatach == 'y':
            meta=mplayer.ShowMetada(yturl[1],mode="plfalse")
            oldhyst=mplayer.LoadTheHystory()
            
            mplayer.UpdateTheHystory(song_name=meta[5],artist=meta[4],genre="None",year="None",comment=meta[6],pllist=False)
            hystresults=mplayer.searchhystory(filename="clean.csv",searchartist=meta[4],plst="off",list=None,)
            mplayer.clearThehystory()
            mplayer.Playing(yturl[1], yturl[0], ytdlst=True, playListMpv="" ,qualitylist=None)
            
        else:
            print("no metadata will be loaded")
            mplayer.Playing(yturl[1], yturl[0], ytdlst=True, playListMpv="" , qualitylist=None)    
    else:
        print("auto mode")
        yturl = mplayer.yt(plmode=False , singlem=True)
        metadatach=input("do you want to see metadata from the links you have choose ? \n")
        if metadatach == 'y':
            meta=mplayer.ShowMetada(yturl[1],mode="plfalse")
            oldhyst=mplayer.LoadTheHystory()
            
            mplayer.UpdateTheHystory(song_name=meta[5],artist=meta[4],genre="None",year="None",comment=meta[6],pllist=False)
            hystresults=mplayer.searchhystory(filename="clean.csv",searchartist=meta[4],plst="off",list=None,)
            mplayer.clearThehystory()
            mplayer.Playing(yturl[1], yturl[0], ytdlst=True, playListMpv="" ,qualitylist=None)
            
        else:
            print("no metadata will be loaded")
            mplayer.Playing(yturl[1], yturl[0], ytdlst=True, playListMpv="" , qualitylist=None)


# this is for all the supported sites (https://ytdl-org.github.io/youtube-dl/supportedsites.html)
elif selmod == "url":
    print("you have selected url mode\n")
    playlistyn = input("you want to make a playlist ? y/n")
    if playlistyn == 'y':
        playList = mplayer.MakePlaylist()
        mplayer.Playing(url=None, quality="",ytdlst=True, playListMpv=playList , qualitylist=None)
        
    if playlistyn == 'n':
        UrlPath = mplayer.TakeUrlMethod()
        mplayer.Playing(url=UrlPath, quality="", ytdlst=True, playListMpv=None ,qualitylist=None)



# this is for local file

elif selmod == "file":
    loadfile="n"
    print("you have selected the local file mode\n")
    playlistyn = input("do you want to make a playlist ? y/n \n")
    if playlistyn == "n":
        print("no playlist")
    elif playlistyn != "y" and playlistyn != "n":
        print("no choice")
    elif playlistyn == "y ":
        loadfile=input("do you want to load folder from your system? \n")
    if playlistyn == "y" and loadfile == "n":
        
        playList = mplayer.MakePlaylist()
        metadata=input("do you want metadate ? \n")
        if metadata =="y":
            metadataextract=mplayer.audiometadata(playList,pllist=True,itemlist=playList)
            oldhystory=mplayer.LoadTheHystory()
            
            hystresults=mplayer.searchhystory(filename="clean.csv",searchartist=metadataextract[4],plst="on",list=metadataextract[5])
            
            
            mplayer.UpdateTheHystory(song_name=metadataextract[4],artist=metadataextract[5],genre=metadataextract[6],year=metadataextract[7],pllist=True,comment=metadataextract[9])
            
            print("this is the comments ")
            print(metadataextract[9])
            
            mplayer.clearThehystory()         
              
        mplayer.Playing(url=None, quality="",
                        ytdlst=False, playListMpv=playList ,qualitylist=None)
    if playlistyn == "y" and loadfile == "y":
        folder=input("give me your folder")
        playList=mplayer.loadonlycomp(listdir=folder)
        metadata=input("do you want metadate ? \n")
        
        if metadata =="y":
            metadataextract=mplayer.audiometadata(playList , pllist=True ,itemlist=playList)
            oldhystory=mplayer.LoadTheHystory()            
           
            mplayer.UpdateTheHystory(song_name=metadataextract[4],artist=metadataextract[5],genre=metadataextract[6],year=metadataextract[7],pllist=True,comment=metadataextract[9])
            mplayer.clearThehystory() 
        mplayer.Playing(url=None, quality="",
                        ytdlst=False, playListMpv=playList ,qualitylist=None)

        print("play list is playing... (i hope) ...")
    elif playlistyn == "n":
        userfile = mplayer.TakeLocalFilePath()
        metadata=input("do you want metadata ? \n")
        if metadata=="y":
            metadataextract=mplayer.audiometadata(userfile,pllist=False,itemlist=None)
            oldhystory=mplayer.LoadTheHystory()
                      
            hystresults=mplayer.searchhystory(filename="output.csv",searchartist=metadataextract[1],plst="off",list=metadataextract[1])
                     
            mplayer.UpdateTheHystory(song_name=metadataextract[0],artist=metadataextract[1],genre=metadataextract[2],year=metadataextract[3],pllist=False,comment=metadataextract[8])
            mplayer.clearThehystory()  
        mplayer.Playing(userfile, quality="", ytdlst=True, playListMpv=None ,qualitylist=None)
        print(type(userfile))
    else:
        print("auto mode")
        userfile = mplayer.TakeLocalFilePath()
        metadata=input("do you want metadata ? \n")
        if metadata=="y":
            metadataextract=mplayer.audiometadata(userfile,pllist=False,itemlist=None)
            oldhystory=mplayer.LoadTheHystory()
                      
            hystresults=mplayer.searchhystory(filename="output.csv",searchartist=metadataextract[1],plst="off",list=metadataextract[1])
                     
            mplayer.UpdateTheHystory(song_name=metadataextract[0],artist=metadataextract[1],genre=metadataextract[2],year=metadataextract[3],pllist=False,comment=metadataextract[8])
            mplayer.clearThehystory()  
        mplayer.Playing(userfile, quality="", ytdlst=True, playListMpv=None ,qualitylist=None)
#this is for "custom_file" mode 
elif selmod == "custom_file":
    print("you are in custom_file mode")
    custList=mplayer.ReadFromFile()
    mplayer.Playing(url=None, quality="",ytdlst=True, playListMpv=custList , qualitylist=None)

    
else:
    print("you didnt penetrate the armor")

