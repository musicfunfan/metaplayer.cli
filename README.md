# What is this ?

This is a Python script that search and play media from youtube.com, local files and other sites, using mpv and yt-dlp.

# Why is here ?
 
* Backup
* share with friends 
* share with the world 

# What can i do with that ?

You can search youtube.com and play videos from there. You can also load local files and other site that yt-dlp supports. Plus you can save the metadata for the media you use in the csv file.

# Is this in active development ?

No i do not really care about this project. But i may updated at some point.

# How can i use it ?

* You need to run some kind of Linux distribution. I use archlinux.
* You need to have this is installed in your system.
* * yt-dlp (latest version)
* * python-mpv
* * tinytag
* * Python (Version 3)

When you install all this run ``python MetaPlayerCli.py``

# Where is the the saved metadata ?

In the script folder there is a clean.csv file there is it.